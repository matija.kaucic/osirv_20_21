import cv2

boats = cv2.imread('../../slike/boats.bmp', 0)
baboon = cv2.imread('../../slike/baboon.bmp', 0)
airplane = cv2.imread('../../slike/airplane.bmp', 0)

slike = [boats, baboon, airplane]
naslovi = ['boats','baboon','airplane']

for i in range(len(slike)):
    ret,thresh1 = cv2.threshold(slike[i],127,255,cv2.THRESH_BINARY)
    cv2.imwrite(naslovi[i] + '_THRESH_BINARY' + '.jpg', thresh1)
    
    ret,thresh2 = cv2.threshold(slike[i],127,255,cv2.THRESH_BINARY_INV)
    cv2.imwrite(naslovi[i] + '_THRESH_BINARY_INV' + '.jpg', thresh2)
    
    ret,thresh3 = cv2.threshold(slike[i],127,255,cv2.THRESH_TRUNC)
    cv2.imwrite(naslovi[i] + '_THRESH_TRUNC' + '.jpg', thresh3)
    
    ret,thresh4 = cv2.threshold(slike[i],127,255,cv2.THRESH_TOZERO)
    cv2.imwrite(naslovi[i] + '_THRESH_TOZERO' + '.jpg', thresh4)
    
    ret,thresh5 = cv2.threshold(slike[i],127,255,cv2.THRESH_TOZERO_INV)
    cv2.imwrite(naslovi[i] + '_THRESH_TOZERO_INV' + '.jpg', thresh5)
    
    thresh6 = cv2.adaptiveThreshold(slike[i],255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    cv2.imwrite(naslovi[i] + '_ADAPTIVE_THRESH_MEAN' + '.jpg', thresh6)
                
    thresh7 = cv2.adaptiveThreshold(slike[i],255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    cv2.imwrite(naslovi[i] + '_ADAPTIVE_THRESH_GAUSSIAN' + '.jpg', thresh7)
    
    ret,thresh8 = cv2.threshold(slike[i],0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    cv2.imwrite(naslovi[i] + '_THRESH_OTSU' + '.jpg', thresh8)