import random

brojac = 1
randNum = random.randint(0, 15)
        
while True: 
    try:
        broj = int(input ('Unesite jedan broj od 0 do 15: '))
        if(broj == randNum):
            print('Broj pokušaja do pogođenog broja: ', brojac)
            break
        else:
            brojac = brojac + 1
        while (broj > 15 or broj < 0):
            print('Niste unijeli odgovarajući broj, unesite broj između 0 i 15')
            break       
    except ValueError:
            print('Niste unijeli broj!')

