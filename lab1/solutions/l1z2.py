import numpy as np
import cv2

path = r'C:\Users\matij\osirv_20_21\lab1\slike\baboon.bmp'

slika = cv2.imread(path)

plava = slika.copy()
plava [:,:,1] = 0
plava [:,:,2] = 0

zelena = slika.copy()
zelena [:,:,0] = 0
zelena [:,:,2] = 0

crvena = slika.copy()
crvena [:,:,0] = 0
crvena [:,:,1] = 0

cv2.imshow('plava', plava)
cv2.imshow('zelena', zelena)
cv2.imshow('crvena', crvena)