lista = []

brojac = 0

while True:
        ulica = input('Unesite ime ulice između 7 i 15 znakova: ')
        if (ulica == "prekid"):
            break
        elif (len(ulica) > 15 or len(ulica) < 7):
            print('Niste unijeli odgovarajući naziv ulice, broj znakova mora biti između 7 i 15. ')
            break 
        try:
            lista.append(ulica)
            brojac = brojac + 1
        except: 
            print('Niste upisali točan naziv ulice.')
            
print('Imena ulica uneseno: ', brojac)
print('Najduže uneseno ime ulice: ', max(lista, key=len))