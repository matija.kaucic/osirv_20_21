import numpy as np
import cv2

path = r'C:\Users\matij\osirv_20_21\lab1\slike\baboon.bmp'

slika = cv2.imread(path)

scale_percent = 50

width1 = int(slika.shape[1])
height1 = int(slika.shape[0] * scale_percent / 100)
dim1 = (width1, height1) 
resized1 = cv2.resize(slika, dim1, interpolation = cv2.INTER_AREA)

width2 = int(slika.shape[1] * scale_percent / 100)
height2 = int(slika.shape[0])
dim2 = (width2, height2)
resized2 = cv2.resize(slika, dim2, interpolation = cv2.INTER_AREA)

width3 = int(slika.shape[1] * scale_percent / 100)
height3 = int(slika.shape[0] * scale_percent / 100)
dim3 = (width3, height3)
resized3 = cv2.resize(slika, dim3, interpolation = cv2.INTER_AREA)

print('Original Dimensions: ', slika.shape)
cv2.imshow("Original image", slika) 

print('Resized Dimensions - pola redaka: ', resized1.shape)
print('Resized Dimensions - pola stupaca: ', resized2.shape)
print('Resized Dimensions - pola redaka i stupaca: ', resized3.shape)
cv2.imshow("Resized image - pola redaka", resized1)  
cv2.imshow("Resized image - pola stupaca", resized2)
cv2.imshow("Resized image - pola redaka i stupaca", resized3)