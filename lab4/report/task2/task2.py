import numpy as np
import cv2
from matplotlib import pyplot as plt

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(255, (1.0 - sigma) * v)) #tu smo samo mijenjali vrijednost (0,128,255)
        upper = int(min(255, (1.0 + sigma) * v)) #tu smo samo mijenjali vrijednost (255,128,255)
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)
        # return the edged image
        return edged

airplane = cv2.imread(r'C:\Users\matij\osirv_20_21\lab3\slike\airplane.bmp', 0)
auto = auto_canny(airplane)
plt.subplot(121),plt.imshow(airplane, cmap='gray')
plt.title('Original Image - airplane'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(auto,  'gray')
plt.title('Edges'), plt.xticks([]), plt.yticks([])
plt.show()
cv2.imwrite('airplane_lower255_upper255.jpg', auto)


barbara = cv2.imread(r'C:\Users\matij\osirv_20_21\lab3\slike\barbara.bmp', 0)
auto = auto_canny(barbara)
plt.subplot(121),plt.imshow(barbara, cmap='gray')
plt.title('Original Image - barbara'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(auto,  'gray')
plt.title('Edges'), plt.xticks([]), plt.yticks([])
plt.show()
cv2.imwrite('barbara_lower255_upper255.jpg', auto)


boats = cv2.imread(r'C:\Users\matij\osirv_20_21\lab3\slike\boats.bmp', 0)
auto = auto_canny(boats)
plt.subplot(121),plt.imshow(boats, cmap='gray')
plt.title('Original Image - boats'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(auto,  'gray')
plt.title('Edges'), plt.xticks([]), plt.yticks([])
plt.show()
cv2.imwrite('boats_lower255_upper255.jpg', auto)


pepper = cv2.imread(r'C:\Users\matij\osirv_20_21\lab3\slike\pepper.bmp', 0)
auto = auto_canny(pepper)
plt.subplot(121),plt.imshow(pepper, cmap='gray')
plt.title('Original Image - pepper'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(auto,  'gray')
plt.title('Edges'), plt.xticks([]), plt.yticks([])
plt.show()
cv2.imwrite('pepper_lower255_upper255.jpg', auto)