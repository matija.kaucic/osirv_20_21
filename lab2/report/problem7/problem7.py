import cv2
import numpy as np

image = cv2.imread('../../slike/baboon.bmp', 0)
image = cv2.resize(image, None, fx=0.25, fy=0.25, interpolation = cv2.INTER_CUBIC) #image scaled to quarter of its height and width
rows, cols = image.shape


image_list = []

for deg in range (0, 360, 30):   # od 0 do 360, step je 30, dakle 12 iteracija s razmakom od 30 stupnjeva
    M = cv2.getRotationMatrix2D((cols/2, rows/2), deg, 1)
    dst = cv2.warpAffine(image, M, (cols, rows))
    image_list.append(dst)


final_image = np.hstack(image_list)
cv2.imwrite('baboon_rotation_stacked.bmp', final_image)