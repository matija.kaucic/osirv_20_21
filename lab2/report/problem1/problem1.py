import numpy as np
import cv2
import PIL


path1 = r'C:\Users\matij\osirv_20_21\lab2\slike\baboon.bmp'
path2 = r'C:\Users\matij\osirv_20_21\lab1\slike\airplane.bmp'
path3 = r'C:\Users\matij\osirv_20_21\lab1\slike\goldhill.bmp'

black = [0, 0, 0]
def Okvir(slika, širina):
    borderBlack = cv2.copyMakeBorder(slika, širina, širina, širina, širina, cv2.BORDER_CONSTANT, value=black )
    return borderBlack

list_im = [path1, path2, path3]
imgs    = [ PIL.Image.open(i) for i in list_im ]
# pick the image which is the smallest, and resize the others to match it (can be arbitrary image shape here)
min_shape = sorted( [(np.sum(i.size), i.size ) for i in imgs])[0][1]
imgs_comb = np.hstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )

imgs_comb = Okvir(imgs_comb, 100)

imgs_comb = PIL.Image.fromarray( imgs_comb)
imgs_comb.save( 'problem1.bmp' )    

# # for a vertical stacking it is simple: use vstack
# imgs_comb = np.vstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )
# imgs_comb = PIL.Image.fromarray( imgs_comb)
# imgs_comb.save( 'Trifecta_vertical.jpg' )