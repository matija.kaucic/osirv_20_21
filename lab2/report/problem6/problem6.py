import numpy as np
import cv2

image = cv2.imread('../../slike/BoatsColor.bmp', 0)

for q in range(1, 9):
    d = 2 ** (8 - q)
    noise = np.random.uniform(0, 1, image.shape)
    noise_img = image.copy()
    noise_img = noise_img.astype(np.float32)
    noise_img = (np.floor(noise_img / d + noise) + 0.5) * d
    noise_img[noise_img > 255] = (np.floor(255 / d) + 0.5) * d
    noise_img[noise_img < 0] = (np.floor(0 / d) + 0.5) * d
    noise_img = noise_img.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + 'n.bmp', noise_img)

#Komentar: Što je q veći, to je slika kvalitetnija. Kao i u prošlom zadatku, za q > 4, razlika nije uočljiva. Na q=4 vidi se razlika u svjetlini i općenitoj kvaliteti slike. Na q=3 sve to je još više naglašeno. Na q=2 slika postaje jako muljava. Na q=1 gube se obrisi slike i objekti su neprepoznatljivi. 