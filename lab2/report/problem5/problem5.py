import cv2
import numpy as np

image = cv2.imread('../../slike/BoatsColor.bmp', 0)

for q in range(1, 9):
    d = 2 ** (8 - q)
    quant_img = image.copy()
    quant_img = quant_img.astype(np.float32)
    quant_img = (np.floor(quant_img / d) + 0.5) * d
    quant_img[quant_img > 255] = 255
    quant_img[quant_img < 0] = 0
    quant_img = quant_img.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + '.bmp', quant_img)

#Komentar: Što je q veći, to je slika kvalitetnija. Razlika između q=8, 7, 6 i 5 se ne vidi. Za q=4 vidi se na nebu, a za niže se primijeti razlika. 