import cv2

image1 = cv2.imread('../../slike/airplane.bmp', 0)
image2 = cv2.imread('../../slike/baboon.bmp', 0)
image3 = cv2.imread('../../slike/barbara.bmp', 0)
image4 = cv2.imread('../../slike/barbara.pgm', 0)
image5 = cv2.imread('../../slike/boats.bmp', 0)
image6 = cv2.imread('../../slike/BoatsColor.bmp', 0)
image7 = cv2.imread('../../slike/goldhill.bmp', 0)
image8 = cv2.imread('../../slike/lenna.bmp', 0)
image9 = cv2.imread('../../slike/pepper.bmp', 0)


image1_inv = 255 - image1
image2_inv = 255 - image2
image3_inv = 255 - image3
image4_inv = 255 - image4
image5_inv = 255 - image5
image6_inv = 255 - image6
image7_inv = 255 - image7
image8_inv = 255 - image8
image9_inv = 255 - image9


cv2.imwrite('Airplane_invert.bmp', image1_inv)
cv2.imwrite('Baboon_invert.bmp', image2_inv)
cv2.imwrite('Barbara_invert.bmp', image3_inv)
cv2.imwrite('Barbara_invert.pgm', image4_inv)
cv2.imwrite('boats_invert.bmp', image5_inv)
cv2.imwrite('BoatsColor_invert.bmp', image6_inv)
cv2.imwrite('goldhill_invert.bmp', image7_inv)
cv2.imwrite('lenna_invert.bmp', image8_inv)
cv2.imwrite('pepper_invert.bmp', image9_inv)
