import cv2

image1 = cv2.imread('../../slike/airplane.bmp', 0)
image2 = cv2.imread('../../slike/baboon.bmp', 0)
image3 = cv2.imread('../../slike/barbara.bmp', 0)
image4 = cv2.imread('../../slike/barbara.pgm', 0)
image5 = cv2.imread('../../slike/boats.bmp', 0)
image6 = cv2.imread('../../slike/BoatsColor.bmp', 0)
image7 = cv2.imread('../../slike/goldhill.bmp', 0)
image8 = cv2.imread('../../slike/lenna.bmp', 0)
image9 = cv2.imread('../../slike/pepper.bmp', 0)

threshold = [63, 127, 191]

for thr in threshold:
    thr_img = image1.copy()
    thr_img[thr_img < thr] = 0
    cv2.imwrite(('airplane_' + str(thr) + '_thresh.png'), thr_img)

for thr in threshold:
    thr_img = image2.copy()
    thr_img[thr_img < thr] = 0
    cv2.imwrite(('baboon_' + str(thr) + '_thresh.png'), thr_img)
    
for thr in threshold:
    thr_img = image3.copy()
    thr_img[thr_img < thr] = 0
    cv2.imwrite(('barbara_' + str(thr) + '_thresh.png'), thr_img)

for thr in threshold:
    thr_img = image4.copy()
    thr_img[thr_img < thr] = 0
    cv2.imwrite(('barbara2_' + str(thr) + '_thresh.png'), thr_img)
 
for thr in threshold:
    thr_img = image5.copy()
    thr_img[thr_img < thr] = 0
    cv2.imwrite(('boats_' + str(thr) + '_thresh.png'), thr_img)   
    
for thr in threshold:
    thr_img = image6.copy()
    thr_img[thr_img < thr] = 0
    cv2.imwrite(('BoatsColor_' + str(thr) + '_thresh.png'), thr_img)
    
for thr in threshold:
    thr_img = image7.copy()
    thr_img[thr_img < thr] = 0
    cv2.imwrite(('goldhill_' + str(thr) + '_thresh.png'), thr_img)
    
for thr in threshold:
    thr_img = image8.copy()
    thr_img[thr_img < thr] = 0
    cv2.imwrite(('lenna_' + str(thr) + '_thresh.png'), thr_img)
    
for thr in threshold:
    thr_img = image9.copy()
    thr_img[thr_img < thr] = 0
    cv2.imwrite(('pepper_' + str(thr) + '_thresh.png'), thr_img)