import numpy as np
import cv2

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output


kernel_boxBlur = 1/9*np.array([[1,1,1], [1,1,1], [1,1,1]])
kernel_sharpen = np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])

image = cv2.imread('../../slike/baboon.bmp', cv2.IMREAD_GRAYSCALE)

konvolucija_boxBlur = convolve(image, kernel_boxBlur)
konvolucija_sharpen = convolve(image, kernel_sharpen)

cv2.imshow('Box blur konvolucija', konvolucija_boxBlur)
cv2.imwrite('BoxBlur.bmp', konvolucija_boxBlur)

cv2.imshow('Sharpen konvolucija', konvolucija_sharpen)
cv2.imwrite('Sharpen.bmp', konvolucija_sharpen)


